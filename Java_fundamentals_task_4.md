## Java Fundamentals. Task #4

## Общие требования
1. Код приложения должен быть отформатирован в едином стиле и соответствовать Java Code Convention.
2. Если приложение содержит консольные меню или ввод/вывод, то они должны быть минимальными, достаточными и интуитивно понятными. Язык – английский.
3. Приложение должно быть работоспособным - т.е. запускаться без дополнительных манипуляций.
4. Результат каждой операции, которую требуется вывести в консоль должен выводится на новую строку.

## Задание
Создать консольное приложение реализующее следующий функционал:

1. Приложение считывает размерность матрицы a [n] [n]. Валидирует входные данные.
2. Заполняет значения элементов матрицы в интервале значений от -M до M с помощью генератора случайных чисел (класс Random). 
3. Упорядочивает строки (столбцы) матрицы в порядке возрастания значений элементов k-го столбца (строки). Выводит полученную матрицу в консоль. 
4. Находит сумму элементов матрицы, расположенных между первым и вторым положительными элементами каждой строки. 
   Выводит номер строки, первый и второй положительный элемент и сумму элементов между ними в консоль.

    пример вывода: 
    ```
    1. Elements: 5, 9; Sum: 65;
    2. Elements: 1, 2; Sum: 2;
    ...
    ```

5. Находит максимальный элемент в матрице и удалить из матрицы все строки и столбцы, его содержащие. Выводит полученную матрицу в консоль.
