import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        System.out.println("Enter n-integer (matrix's size):");
        int size = getValidInteger();
        int[][] matrix = initMatrix(size);
        sortMatrix(matrix);
        printMatrix(matrix);
//        System.out.println("```");
        findSumOfPositiveElements(matrix);
//        System.out.println("```");
        System.out.println(getMaxElement(matrix));
        deleteMaxElementFromMatrix(matrix, size, getMaxElement(matrix));
    }

    //Проверка на ввод целых чисел больше 0
    public static int getValidInteger() {
        Scanner sc = new Scanner(System.in);
        int tmpInt = sc.nextInt();
        if (tmpInt < 0) {
            throw new IllegalArgumentException("Entered integer must be greater than zero");
        } else {
            return tmpInt;
        }
    }

    //Рандомайзер для заполнения матрицы
    public static int getRandomInteger(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Max must be greater than min!");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    //Вывод в консоль матрицы
    public static void printMatrix(int[][] matrix) {
        for (int m = 0; m < matrix.length; m++) {
            for (int n = 0; n < matrix[m].length; n++) {
                System.out.print(matrix[m][n] + "\t");
            }
            System.out.println();
        }
    }

    //Заполнение матрицы случайными числами (диапазон рандома от -9 до 9)
    public static int[][] initMatrix(int size) {
        int[][] tmpMatrix = new int[size][size];
        for (int m = 0; m < size; m++) {
            for (int n = 0; n < size; n++) {
                tmpMatrix[m][n] = getRandomInteger(-9, 9);
            }
        }
        return tmpMatrix;
    }

    //сортирует строку в зависимости возрастания значения элемента столбца
    public static void sortMatrix(int[][] matrix) {
        int temp;
        for (int m = 0; m < matrix.length; m++) {
            for (int n = matrix[m].length -1; n >= 1; n--) {
                for (int k = 0; k < n; k++) {
                    if (matrix[m][k] > matrix[m][k + 1]) {
                        temp = matrix[m][k];
                        matrix[m][k] = matrix[m][k + 1];
                        matrix[m][k + 1] = temp;
                    }
                }
            }
        }
    }

    //Поиск суммы элементов между 1-ым и 2-ым положительным элементом строки матрицы
    public static void findSumOfPositiveElements(int[][] matrix) {
        int firstPositiveIndex = 0;
        int secondPositiveIndex = 0;
        for (int m = 0; m < matrix.length; m++) {
            for (int n = 0; n < matrix[m].length; n++) {
                if (matrix[m][n] > 0) {
                    firstPositiveIndex = n;
                    break;
                }
            }
            for (int n = matrix[m].length - 1; n >= firstPositiveIndex; n--) {
                if (matrix[m][n] > 0) {
                    secondPositiveIndex = n;
                    break;
                }
            }
            int sum = matrix[m][firstPositiveIndex];
            for (int n = firstPositiveIndex + 1; n <= secondPositiveIndex; n++) {
                sum += matrix[m][n];
            }
            System.out.println((m + 1) + ". Elements: " + matrix[m][firstPositiveIndex] + ", " +
                               matrix[m][secondPositiveIndex] + "; Sum: " + sum + ";");
        }
    }

    //Поиск максимального элемента матрицы
    public static int getMaxElement(int[][] matrix) {
        int maxElement = matrix[0][0];
        for (int m = 0; m < matrix.length; m++) {
            for (int n = 0; n < matrix[m].length; n++) {
                if (maxElement < matrix[m][n]) {
                    maxElement = matrix[m][n];
                }
            }
        }
        return maxElement;
    }

    //Удаление строки и столбца, содержащих максимальный элемент матрицы
    public static void deleteMaxElementFromMatrix(int[][] matrix, int n, int maxElement) {
        int linesIndex = 0;
        int rowsIndex = 0;
        int newLines = 0;
        int newColumns = 0;
        for (int m = 0; m < matrix.length; m++) {
            for (int i = 0; i < matrix[m].length; i++) {
                if (matrix[m][i] == maxElement) {
                    linesIndex = m;
                    rowsIndex = i;
                    break;
                }
            }
        }
        int[][] newMatrix = new int[n - 1][n - 1];
        for (int m = 0; m < matrix.length; m++) {
            if (m != linesIndex) {
                for (int i = 0; i < matrix[m].length; i++) {
                    if (i != rowsIndex) {
                        newMatrix[newLines][newColumns] = matrix[m][i];
                        newColumns++;
                    }
                }
                newColumns = 0;
                newLines++;
            }
        }
        printMatrix(newMatrix);
    }
}